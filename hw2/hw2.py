import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as st

def make_markdown_table(array):

    """ Input: Python list with rows of table as lists
               First element as header. 
        Output: String to put into a .md file 
        
    Ex Input: 
        [["Name", "Age", "Height"],
         ["Jake", 20, 5'10],
         ["Mary", 21, 5'7]] 
    """


    markdown = "\n" + str("| ")

    for e in range(len(array[0])):
        to_add = " " + f"{e:.2f}" + str(" |")
        markdown += to_add
    markdown += "\n"

    markdown += '|'
    for i in range(len(array[0])):
        markdown += str("-------------- | ")
    markdown += "\n"

    for entry in array:
        markdown += str("| ")
        for e in entry:
            to_add = f"{e:.2f}" + str(" | ")
            markdown += to_add
        markdown += "\n"

    return markdown + "\n"

# Function to estimate word length probabilities
def estimate_word_length_probabilities(text):
    word_lengths = [len(word) for word in text.split()]
    total_words = len(word_lengths)
    word_length_counts = {length: word_lengths.count(length) / total_words for length in set(word_lengths)}
    return word_length_counts

def get_word_length_histogram():
    # Estimate word length probabilities for each text
    word_length_probabilities = []
    for text in texts:
        word_length_probabilities.append(estimate_word_length_probabilities(text))

    # Plot the word length distribution for each text
    for i, probabilities in enumerate(word_length_probabilities):
        plt.figure()  # create a new figure
        plt.bar(probabilities.keys(), probabilities.values(), alpha=0.5)
        plt.xlabel('Délka slova')
        plt.ylabel('Pravděpodobnost')
        plt.ylim(0, 0.3)
        plt.xlim(0, 14)
        plt.title(label=f'Pravděpodobnost délky slova v textu {file_paths[i].split(".")[0]}')
        plt.savefig(f'word_length_distribution_{file_paths[i].split(".")[0]}.png')  # save each figure to a separate file

    return word_length_probabilities

def mean_and_variance(word_length_probabilities, texts):
    means = []
    variances = []

    for i, text_probabilities in enumerate(word_length_probabilities):
        # Mean EX
        mean = 0
        for key, value in text_probabilities.items():
            mean += key * value
        means.append(mean)
        print(f"""Mean value of word length in file {file_paths[i].split(".")[0]} is {mean:.4f}""")

        # Variance
        var = 0
        for key, value in text_probabilities.items():
            var += key * key * value
        variance = (var - mean**2)
        
        variances.append(variance)
        print(f"""Variance of word length in file {file_paths[i].split(".")[0]} is {variance:.4f}\n""")

    return means, variances

def equality_test_statistic(texts):
    matrix = []
    for text in texts:
        word_lengths = [len(word) for word in text.split()]
        temp = []
        for i in range( max(word_lengths) +1):
            temp.append(0)
        for i in range( len(word_lengths) ):
            temp[word_lengths[i]] += 1
        matrix.append(temp)
    
    x = np.array(matrix[0])
    y = np.array(matrix[1])
    
    print(x)
    print(y)
    #nepředpokládáme rovnost rozptylů
    n = len(x)
    m = len(y)
    Xn = np.mean(x)
    Ym = np.mean(y)
    sX2 = np.var(x, ddof=1)
    sY2 = np.var(y, ddof=1)
    sX = np.std(x, ddof=1)
    sY = np.std(y, ddof=1)
    
    alpha = 0.05
    sd2 = (sX2/n + sY2/m)
    nd = sd2**2/((sX2/n)**2/(n-1) + (sY2/m)**2/(m-1))
    testT = (Xn - Ym)/np.sqrt(sd2)
    t = st.t.isf(alpha/2,nd)
    p = 2*st.t.sf(np.abs(testT),nd) # = 2*(1 - st.t.cdf(np.abs(T),nd))
    print("T = ", testT, ", t = ", t , ", nd = ", nd, sep="")
    print("p = ", p)
    

def independence_test_statistic(texts):
    matrix = []
    for text in texts:
        word_lengths = [len(word) for word in text.split()]
        temp = []
        for i in range( max(word_lengths) +1):
            temp.append(0)
        for i in range( len(word_lengths) ):
            temp[word_lengths[i]] += 1
        matrix.append(temp)
    
    matrix[0].append(0)
    matrix[0].append(0)

    Nij = np.array(matrix)
    print(Nij)
        
    n = np.sum(Nij)
    # print("Nij =\n", Nij)
    # print("Nij-shape =\n", Nij.shape)
    # print("n =", n)
    pi_ = (np.sum(Nij, axis = 1))/n
    p_j = (np.sum(Nij, axis = 0))/n
    
    pi = [[pi_[0]], [pi_[1]]]
    pi_ = np.array(pi)
    
    pj = [p_j]
    p_j = np.array(pj)
    
    # print("pi_ =\n", pi_)
    # print("pi_-shape =\n", pi_.shape)
    # print("p_j =\n", p_j)
    # print("p_j-shape =\n", p_j.shape)
    
    pipj = np.matmul(pi_,p_j)
    npipj = n * pipj
    
    print("npipj =\n",npipj)
    print("npipj.shape =\n",npipj.shape)
    
    

    #slučujeme poslední čtyři sloupce a první taky asi idk

    #vynechání prvného slupce (slova nulové délky(nejsou))
    Nij = Nij[:,1:]
    
    #vynecháme poslední 4 sloupce které nahradíme jejich součtem
    temp = np.sum(Nij[:, -4:], axis=1)
    
    t = [[temp[0]], [temp[1]]]
    temp = np.array(t)
    
    Nij = Nij[:, :-4]
    Nij = np.append(Nij, temp, axis=1)    

    print(Nij)
    
    n = np.sum(Nij)
    pi_ = (np.sum(Nij, axis = 1))/n
    p_j = (np.sum(Nij, axis = 0))/n
    
    pi = [[pi_[0]], [pi_[1]]]
    pi_ = np.array(pi)
    
    pj = [p_j]
    p_j = np.array(pj)
    
    pipj = np.matmul(pi_,p_j)
    npipj = n * pipj
    
    print("npipj =\n",npipj)
    print("npipj.shape =\n",npipj.shape)
    
    print(make_markdown_table(npipj))
    
    Chi2 = np.sum(np.square(Nij - npipj)/npipj)
    print("Chi2 =", Chi2)
    alpha = 0.05
    df = (np.size(Nij,axis =0) - 1)*(np.size(Nij,axis =1) - 1)
    print("df =",df)
    chi2 = st.chi2.isf(alpha,df)
    print("chi2 =", chi2)
    p = st.chi2.sf(Chi2,df) # = 1-st.chi2.cdf(Chi2,df)
    print("p =", p)
    
    # pomocí funkce
    Chi2, p, df, _ = st.chi2_contingency(Nij, correction = False)
    print("Chi2 =", Chi2)
    print("df =",df)
    print("p =", p)
    
    # 36 > 18 => zamítáme H0
    # p hodnota je 4.4 < 5 => zamítáme H0

# Read text from files
file_paths = ['016.txt', '002.txt']
texts = []
for file_path in file_paths:
    with open(file_path, 'r') as file:
        file.readline()
        text = file.read()
        texts.append(text)

#word_length_probabilities = get_word_length_histogram()
#means, variances = mean_and_variance(word_length_probabilities, texts)
#independence_test_statistic(texts)
equality_test_statistic(texts)
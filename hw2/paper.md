# NI-VSM

## Domácí úkol 2

autoři: Daniel Ridzoň, Jan Pecka, Miloš Pánek 

## Datová sada

Reprezentant: Jan Pecka

- K = 13

- L = 5

- X = 16, Y = 2

zkoumáme tedy soubory 016.txt a 002.txt.

## Programy a knihovny

Pro zpracování dat jsme zvolili programovací jazyk python a používáme knihovny numpy, scipy, a matplotlib.

## 1)
Pro každé slovo jsme spočítali jeho délku a následně jsme spočítali pravděpodobnost každé délky jako počet výskytů děleno počet slov v textu.

$$
p(i) = \frac{n(i)}{N}
$$

Kde $n(i)$ je počet výskytů slova délky $i$ a $N$ je délka textu ve slovech.

![histogram dělek slov textu 016](word_length_distribution_016.png)
![histogram dělek slov textu 002](word_length_distribution_002.png)

## 2)

Střední délku jsme odhadli jako průměr délek slov vážený jejich pravděpodobností.
$$
\mu = \sum_{i=1}^{n} p(i) \cdot i
$$

Kde $p(i)$ je pravděpodobnost výskytu slova délky $i$.

Rozptyl jsme odhadly jako průměr druhých mocnin  délek slov vážený jejich pravděpodobností.

$$
\sigma^2 = \sum_{i=1}^{n} p(i) \cdot i^2 - \mu^2
$$

```python
# Mean
mean = 0
for length, probability in text_probabilities.items():
    mean += length * probability

# Variance
var = 0
for length, probability in text_probabilities.items():
    var += length * length * probability

variance = (var - mean**2)

```
Text 016:

$\mu_{16}$ =  4.0856

$\sigma^2_{16}$ = 5.1551

Text 002:

$\mu_{2}$ = 3.9792 

$\sigma^2_{2}$ = 3.7392


## 3)

$H0:$ Rodělení délek slov je nezávislé na textu

$HA:$ Rodělení délek slov není nezávislé na textu


Postupujeme pomocí testu nezávislosti v kontingenční tabulce.

![Test v kontigenční tabulce](image-1.png)

\pagebreak

| $l:$ | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 |
|--|-------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | 
| *$f(l):$* | 0.0 | 54.7 | 228.9 | 346.2 | 241.7 | 129.4 | 74.1 | 72.4 | 45.9 | 26.5 | 12.1 | 12.1 | 3.8 | 1.1 | 0.5 |
| $f(l):$ | 0.0 | 44.2 | 185.0 | 279.7 | 195.3 | 104.5 | 59.8 | 58.5 | 37.0 | 21.4 | 9.8 | 9.8 | 3.1 | 0.8 | 0.4 |

$f(l)$ je teoretická četnost slov délky $l$.

Pro dobré fungovaní testu je potřeba vstupní data upravit, protože se v tabulce vyskytují délky slov s příliš nízkou četností (méně než 5). Vynecháme tedy první sloupec (slova délky 0) a poslední čtyři sloupce sečteme v jeden.

| $l:$ | 0, 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11+ |
|-|-------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | 
| $f(l):$ | 54.7 | 228.9 | 346.2 | 241.7 | 129.4 | 74.1 | 72.4 | 45.9 | 26.5 | 12.1 | 17.7 |
| $f(l):$ | 44.2 | 185.0 | 279.7 | 195.3 | 104.5 | 59.8 | 58.5 | 37.0 | 21.4 | 9.8 | 14.3 |

Následně jsme určili hodnotu testové statistiky podle vzorce výše.

$\chi^2 \doteq 37.61$

$\chi^2_{0.05, 10} \doteq 18.30$

Protože $\chi^2$ > $\chi^2_{0.05, 10}$ spadá testová statistika do krtitického oboru testu a tedy zamítáme nulovou hypotézu ve prospěch alternativní na hladině významnosti 5 %.

p-hodnota testu je rovna 4.42 e-5.

## 4)

$H0:$ Střední dělky slov obou textů se rovnají

$HA:$ Střední dělky slov obou textů se nerovnají

Pro ověření používáme dvouvýběrový test. Tento test používámě, protože zkoumáme dva nezávislé texty, nikoliv například dva texty od stejného autora.

Pro test nepředpokládáme rovnost rozptylů délek slov jednotlivých textů, jedná se totiž o různé autory.

![Dvouvýběrový test](image-2.png)

Ověřujeme tedy zda $|T| \ge t_{\alpha/2,n_d}$ 

$T \doteq 0.76$

$n_d \doteq 23$

$t_{0.025, 23} \doteq 2.068$

Protože $|T| \ngeq t_{\alpha/2,n_d}$ nezamítáme $H_0$ na hladině významnosti 5 %.

p-hodnota testu je přibližně 45 %.
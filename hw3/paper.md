# NI-VSM

## Domácí úkol 3

autoři: Daniel Ridzoň, Jan Pecka, Miloš Pánek 

## Datová sada

Reprezentant: Jan Pecka

- K = 13

- L = 5

- X = 16, Y = 2

zkoumáme tedy soubory 016.txt a 002.txt.

Text 002.txt považujeme za první text, 016.txt za druhý

## 1)

Postupujeme pomocí metody četností přechodů popsané na přednášce 17.

```python
def transition_matrix(text):
    matrix = np.zeros(shape=(27, 27))
    
    #Pro každý znak v textu vypočítej počet přechodů jednoho znaku na druhý   
    for i in range(len(text)-1):
        x = toInt(text[i])
        y = toInt(text[i+1])
        matrix[x,y] += 1
    
    #normalizuj řádky matice
    for i in range(len(matrix)):
        matrix[i] = matrix[i]/np.sum(matrix[i])

    #vytvoření heatmapy
    plt.figure(figsize=(20, 10))
    plt.title("Matice přechodu", fontsize=40)
    hm = sns.heatmap(data=matrix, 
                annot=True,
                xticklabels=labels,
                yticklabels=labels,
                fmt=".2f")
    plt.savefig("matrix.png")
    
    return matrix
```

![Matice přechodů pro soubor 002.txt](matrix.png)

\pagebreak

## 2)

Při hledání stacionárního rozdělení řešíme soustavu rovnic:
$$
\pi \cdot \mathbf{P} = \pi
$$
$$
\sum_{i \in S} \pi_{i} = 1
$$

```python
#z notebooku pro 7. cvičení
def stationary(matrix):
    W = np.transpose(matrix-np.eye(matrix.shape[0])) 
    pi = la.null_space(W)
    pi = np.transpose(pi/sum(pi)) # normalizace
    return pi[0]
```

Nalezené stacionární rozdělení:

<div align="left">

| $\mathbf{i:}$ | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
|-----|-------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- |
| $\mathbf{\pi_i:}$ |0.2008 | 0.0682 | 0.0133 | 0.0145 | 0.0360 | 0.0951 | 0.0151 | 0.0169 | 0.0559 |

| 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 |
|-------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- |
| 0.0525 | 0.0010 | 0.0093 | 0.0342 | 0.0147 | 0.0523 | 0.0622 | 0.0121 | 0.0004 |

| 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 |
|-------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- |
| 0.0424 | 0.0489 | 0.0832 | 0.0229 | 0.0068 | 0.0261 | 0.0002 | 0.0145 | 0.0004 | 

</div>

\pagebreak

Ověříme, zda se jedná o stacionární rozdělení:

```python
print("pi * M = ", np.dot(pi, matrix))
print("pi     = ", pi)

print("pi == pi * M", abs(pi - np.dot(pi, matrix)) < 0.000001) #prints True
print(np.sum(pi)) #prints 0.9999999999999999
```

Nalezené rozdělení tedy skutečně splňuje vlastnosti stacionárního rozdělení pokud vezmeme v úvahu nepřesnosti způsobené zaokrouhlováním. 

## 3)

$H0:$ Rozdelení znaků souboru 016.txt se rovná nalezenému stacionárnimu rozdelení pi.

$HA:$ Rozdelení znaků souboru 016.txt se nerovná nalezenému stacionárnimu rozdelení pi.

Postupujeme pomocí testu dobré shody.

```python
def test(pi, text, alpha):
    #výpočet teoretických četností
    n = len(text)
    npi = n * pi
    npi = sorted(npi)
    
    #musíme spojit biny aby v každém byla hodnota alespoň 5
    npi = [sortednpi[0] + sortednpi[1] + sortednpi[2] + sortednpi[3]] + sortednpi[4:]
        
    dist_arr = char_count(text)
    dist_arr = sorted(dist_arr)

    #musíme spojit biny aby v každém byla hodnota alespoň 5 a aby byly shodné délky
    dist_arr = [dist_arr[0] + dist_arr[1]+ dist_arr[2]+ dist_arr[3]] + dist_arr[4:]

    #výpočet testové statistiky pomocí stats knihovny
    chi2, p = stats.chisquare(dist_arr, npi)
    
    print(chi2)
    #výpočet testové statistiky pomocí vzroce (pro kontrolu)
    sum = 0
    for i in range(len(dist_arr)):
        sum += ((dist_arr[i] -nPi[i])**2)/nPi[i]
        
    print(sum)
    
    print("p: ", p * 100, " %")
    
    if(p > alpha):
        print('H0 nezamítáme.')
    else:
        print('H0 zamítáme ve prospěch HA.')

```

Testová statistika vychází 88.439, p hodnota testu 1.297 e-9. Kritická hodnota testu pro 23 stupňů volnosti a hladinu významnosti 5 % je 35.172.

Zamítáme tedy nulovou hypotézu ve prospěch hypotézy alternativní na hladině významnosti 5 %.
import matplotlib.pyplot as plt
from collections import Counter
import numpy as np
import seaborn as sns
import scipy.linalg as la
import scipy.stats as stats

labels = []

def make_markdown_table(array):

    """ Input: Python list with rows of table as lists
               First element as header. 
        Output: String to put into a .md file 
        
    Ex Input: 
        [["Name", "Age", "Height"],
         ["Jake", 20, 5'10],
         ["Mary", 21, 5'7]] 
    """


    markdown = "\n" + str("| ")

    for e in range(len(array[0])):
        to_add = " " + f"{e:.2f}" + str(" |")
        markdown += to_add
    markdown += "\n"

    markdown += '|'
    for i in range(len(array[0])):
        markdown += str("-------------- | ")
    markdown += "\n"

    for entry in array:
        markdown += str("| ")
        for e in entry:
            to_add = f"{e:.4f}" + str(" | ")
            markdown += to_add
        markdown += "\n"

    return markdown + "\n"

def load_text(filename):
    with open(filename, 'r', encoding='utf-8') as file:
        file.readline()
        return file.read()

def calculate_probabilities(text):
    counter = Counter(text)
    total_characters = sum(counter.values())
    probabilities = {char: count / total_characters for char, count in counter.items()}
    return probabilities

def char_count(text):
    arr = np.zeros(shape=(27))
    for char in text:
        arr[toInt(char)] += 1
    return arr

def toInt(char):
    if char == ' ': return 0
    return ord(char)-ord('a') + 1

def transition_matrix(text):
    matrix = np.zeros(shape=(27, 27))
    
    #Pro každý znak v textu vypočítej počet přechodů jednoho znaku na druhý   
    for i in range(len(text)-1):
        x = toInt(text[i])
        y = toInt(text[i+1])
        matrix[x,y] += 1
    
    #normalizuj řádky matice
    for i in range(len(matrix)):
        matrix[i] = matrix[i]/np.sum(matrix[i])

    #vytvoření heatmapy
    plt.figure(figsize=(25, 10))
    plt.title("Matice přechodu", fontsize=40)
    hm = sns.heatmap(data=matrix, 
                annot=True,
                xticklabels=labels,
                yticklabels=labels,
                fmt=".3f")
    plt.savefig("matrix.png")
    
    return matrix

#np.set_printoptions(suppress=True)


def stationary(matrix):
    W = np.transpose(matrix-np.eye(matrix.shape[0])) 
    pi = la.null_space(W)
    pi = np.transpose(pi/sum(pi)) # normalizace
    return pi[0]

def test(pi, text, alpha):
    #výpočet teoretických četností
    n = len(text)
    npi = n * pi
    sortednpi = sorted(npi)
    
    #musíme spojit biny aby v každém byla hodnota alespoň 5
    mergedNpi = [sortednpi[0] + sortednpi[1] + sortednpi[2] + sortednpi[3]] + sortednpi[4:]
    nPi = mergedNpi
    
    dist_arr = char_count(text)
    dist_arr = sorted(dist_arr)

    #musíme spojit biny aby v každém byla hodnota alespoň 5 a aby byly shodné délky
    dist_arr = [dist_arr[0] + dist_arr[1]+ dist_arr[2]+ dist_arr[3]] + dist_arr[4:]

    #výpočet testové statistiky pomocí stats knihovny
    chi2, p = stats.chisquare(dist_arr, nPi)
    
    print(chi2)
    #výpočet testové statistiky pomocí vzroce (pro kontrolu)
    sum = 0
    for i in range(len(dist_arr)):
        sum += ((dist_arr[i] -nPi[i])**2)/nPi[i]
        
    print(sum)
    
    print("p: ", p * 100, " %")
    
    if(p > alpha):
        print('H0 nezamítáme.')
    else:
        print('H0 zamítáme ve prospěch HA.')

if __name__ == '__main__':
    labels.append(' ')
    for i in range(26):
        labels.append(chr(i + 97))
    
    text016 = load_text('016.txt')
    text002 = load_text('002.txt')
    

    probabilities016 = calculate_probabilities(text016)
    probabilities002 = calculate_probabilities(text002)
    
    matrix = transition_matrix(text002)
    pi = stationary(matrix)
    
    np.set_printoptions(suppress=True, precision=4)
    
    print("pi * M = ", np.dot(pi, matrix))
    print("pi     = ", pi)
    print("pi == pi * M", abs(pi - np.dot(pi, matrix)) < 0.000001)
    print(np.sum(pi))
    
    print(make_markdown_table([pi]))
    
    probabilities016array = np.zeros(27)
    for (char, prob) in probabilities016.items():
        i = toInt(char)
        probabilities016array[i] = prob
    
    #print(probabilities016array)
    test(pi, text002, 0.05)
    
    
    
# NI-VSM

## Domácí úkol 1

autoři: Daniel Ridzoň, Jan Pecka, Miloš Pánek 

## Datová sada

Reprezentant: Jan Pecka

- K = 13

- L = 5

- X = 16, Y = 2

zkoumáme tedy soubory 016.txt a 002.txt.

## Programy a knihovny

Pro zpracování dat jsme zvolili programovací jazyk python a používáme knihovny numpy a matplotlib.

## 1)

$$
p(x_i) = \frac{n(x_i)}{N}
$$

Kde $n(x_i)$ je počet výskytů prvku $x_i$ a $N$ je délka textu ve znacích.

![histogram znaků textu 016](016.png)
![histogram znaků textu 002](002.png)

##  2)

Výpočet entropie:
$$
H(X) = -\sum_{i} p(x_i) \log_2(p(x_i))
$$
```python
def calculate_entropy(probabilities):
    return -sum(p * np.log2(p) for p in probabilities.values() if p > 0)
```
- Entropie souboru *016*: 4.041276204320588 b
- Entropie souboru *002*: 4.037379295589636 b

## 3)

Podle algoritmu popsaného na přednášce jsme vytvořili Huffmanův kód za použití četností z prvního ze souborů (016.txt).

``` python
def huffman(probabilities):
    nodes = []
    for key, value in probabilities.items():
        nodes.append(CNode(key, value))
    
    while len(nodes) != 1:
        nodes = sorted(nodes, key=lambda x: x.probability)
        newNode = CNode( None, nodes[0].probability + nodes[1].probability )
        newNode.first  = nodes.pop(0)
        newNode.second = nodes.pop(0)
        newNode.first.parent = newNode
        newNode.second.parent = newNode
        nodes.append(newNode)
    
    codes = {}
    queue = []
    queue.append(nodes[0])
    while len(queue):
        node = queue.pop(0)
        if node.first == None:
            codes[node.char] = node.code
            continue
        node.first.code = node.code + "1"
        node.second.code = node.code + "0"
        queue.append(node.first)
        queue.append(node.second)    
    
    return codes 
```
Výsledný kód:

| Znak | Kód $C_{016}$ |Pravděpodobnost znaku|
|------|-----------|---|
| | 11| 0.1965|
|e| 101| 0.1004|
|s| 1001| 0.0521|
|h| 1000| 0.0541|
|o| 0101| 0.0672|
|t| 0100| 0.0713|
|a| 0011| 0.0716|
|u| 01110| 0.0297|
|l| 01101| 0.0302|
|d| 01100| 0.0323|
|r| 00011| 0.0447|
|n| 00001| 0.0488|
|i| 00000| 0.0505|
|p| 011111| 0.0143|
|g| 011110| 0.0143|
|f| 001011| 0.0157|
|y| 001001| 0.0206|
|w| 001000| 0.0208|
|m| 000101| 0.0212|
|c| 000100| 0.0238|
|b| 0010101| 0.0091|
|v| 00101000| 0.0057|
|k| 001010010| 0.0033|
|q| 0010100110| 0.0013|
|j| 00101001111| 0.0003|
|x| 00101001110| 0.0003|

Výsledný kód je optimální pro kódováýní souboru 016 protože se jedná o Huffmanův kód.

## 4)

Výpočet střední délky kódu:

$$
L = \sum_{i=1}^{n} p(x_i) \cdot l(x_i)
$$

```python
def mean_code_length(probabilities, code):
    sum = 0
    for char, p in probabilities.items():
        if not char in code.keys(): continue
        sum += p * len(code[char])
    return sum
```

- Střední délka kódu $C_{016}$ pro soubor 016: 4.082756450597858
- Střední délka kódu $C_{016}$ pro soubor 002: 4.095067621320604

> Při výpočtu střední délky kódu $C_{016}$ pro soubor 002 nastal problém, protože se v souboru 002 vyskytují znaky, které nejsou v souboru 016. Tyto znaky můžeme uvažovat jako že mají nulovou pravděpodobnost pro účely výpočtu střední délky. Protože je jejich pravděpodobnost nulová, nezáleží na délce jejich kódových slov.

Kód $C_{016}$ není pro kódování textu 002 optimální. Pomocí stejného postupu jako výše jsme totiž nalezli kód $C_{002}$ který má nižší střední délku kódu.

Střední délka kódu $C_{002}$ pro soubor 002: 4.077963404932379

| Znak | Kód $C_{002}$ |Pravděpodobnost znaku|
|------|-----------|---|
| | 11| 0.2007|
|s| 1011| 0.0489|
|n| 1010| 0.0523|
|i| 1001| 0.0525|
|h| 0111| 0.0559|
|o| 0110| 0.0623|
|a| 0100| 0.0682|
|t| 0010| 0.0831|
|e| 0000| 0.0951|
|w| 10001| 0.0261|
|l| 01010| 0.0342|
|d| 00110| 0.0360|
|r| 00011| 0.0424|
|y| 100001| 0.0145|
|c| 100000| 0.0147|
|m| 010111| 0.0147|
|f| 010110| 0.0151|
|g| 001111| 0.0169|
|u| 000101| 0.0229|
|k| 0011100| 0.0093|
|p| 0001001| 0.0121|
|b| 0001000| 0.0133|
|v| 00111010| 0.0068|
|j| 001110111| 0.0010|
|z| 0011101101| 0.0004|
|x| 00111011001| 0.0002|
|q| 00111011000| 0.0004|
import matplotlib.pyplot as plt
from collections import Counter
import numpy as np

# Funkce pro načtení textu ze souboru
def load_text(filename):
    with open(filename, 'r', encoding='utf-8') as file:
        file.readline()
        return file.read()

# Funkce pro výpočet pravděpodobnosti znaků
def calculate_probabilities(text):
    counter = Counter(text)
    total_characters = sum(counter.values())
    probabilities = {char: count / total_characters for char, count in counter.items()}
    return probabilities

# Funkce pro grafické znázornění
def plot_probabilities(probabilities, title, filename):
    # Seřazení znaků podle pravděpodobnosti
    characters, probs = zip(*sorted(probabilities.items(), key=lambda x: x[1], reverse=True))
    
    # Vytvoření grafu
    plt.figure(figsize=(10, 8))
    plt.bar(characters, probs)
    plt.xlabel('Znak')
    plt.ylabel('Pravděpodobnost')
    plt.title(title)
    plt.xticks(rotation='horizontal')
    plt.savefig(f"{filename}.png")

# Funkce pro výpočet entropie rozdělení
def calculate_entropy(probabilities):
    return -sum(p * np.log2(p) for p in probabilities.values() if p > 0)

class CNode:
    def __init__(self, char, probability):
        self.char = char
        self.probability = probability
        self.first = None
        self.second = None
        self.parent = None
        self.code = ""
    def __str__(self):
        return str(self.probability)

def huffman(probabilities):
    nodes = []
    for key, value in probabilities.items():
        nodes.append(CNode(key, value))
    
    while len(nodes) != 1:
        nodes = sorted(nodes, key=lambda x: x.probability)
        newNode = CNode( None, nodes[0].probability + nodes[1].probability )
        newNode.first  = nodes.pop(0)
        newNode.second = nodes.pop(0)
        newNode.first.parent = newNode
        newNode.second.parent = newNode
        nodes.append(newNode)
    
    codes = {}
    queue = []
    queue.append(nodes[0])
    while len(queue):
        node = queue.pop(0)
        if node.first == None:
            codes[node.char] = node.code
            continue
        node.first.code = node.code + "1"
        node.second.code = node.code + "0"
        queue.append(node.first)
        queue.append(node.second)    
    
    return codes    

def mean_code_length(probabilities, code):
    sum = 0
    for char, p in probabilities.items():
        if not char in code.keys(): continue
        sum += p * len(code[char])
    return sum


fielnames = {"016.txt", "002.txt"}
# Načtení textů a výpočet pravděpodobností
if __name__ == '__main__':
    text016 = load_text('016.txt')
    text002 = load_text('002.txt')

    probabilities016 = calculate_probabilities(text016)
    probabilities002 = calculate_probabilities(text002)

    # Grafické znázornění pravděpodobností
    plot_probabilities(probabilities016, 'Pravděpodobnosti znaků v souboru 002.txt', "002")
    plot_probabilities(probabilities002, 'Pravděpodobnosti znaků v souboru 016.txt', "016")
    
    print(f"File 016 entropy: {calculate_entropy(probabilities016)}")
    print(f"File 002 entropy: {calculate_entropy(probabilities002)}\n")
    
    code016 = huffman(probabilities016)    
    code002 = huffman(probabilities002)
    
    code016lentext002 = mean_code_length(probabilities002, code016)
    code016lentext016 = mean_code_length(probabilities016, code016)
    
    code002lentext016 = mean_code_length(probabilities016, code002)
    code002lentext002 = mean_code_length(probabilities002, code002)
    
    print(f"Mean code len code016 on text 016: {code016lentext016}")
    print(f"Mean code len code016 on text 002: {code016lentext002}")
    print(f"Mean code len code002 on text 002: {code002lentext002}")
    print(f"Mean code len code002 on text 016: {code002lentext016}")
    
    print("\n| Znak | Kód C016 |Pravděpodobnost znaku|")
    print("|------|-----------|---|")
    for key, value in code016.items():
        prob = f"{probabilities016[key]:.4f}"
        print(f"|{key}| {value}| {prob}|")
    
    print("\n| Znak | Kód C002 |Pravděpodobnost znaku|")
    print("|------|-----------|---|")
    for key, value in code002.items():
        prob = f"{probabilities002[key]:.4f}"
        print(f"|{key}| {value}| {prob}|")
    
    # for filename in fielnames:
    #     text = load_text(filename)
    #     probabilities = calculate_probabilities(text)
    #     plot_probabilities(probabilities, 'Pravděpodobnosti znaků v souboru 016.txt', filename.split('.')[0])
    
    #     print(f"File 2 entropy: {calculate_entropy(probabilities)}")
    #     code = huffman(probabilities)

    #     for key, value in code.items():
    #         print("{:<3} {:<10}".format(key, value))
        
    #     lol = 
    #     print(lol)
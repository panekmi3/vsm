# NI-VSM

## Domácí úkol 4

autoři: Daniel Ridzoň, Jan Pecka, Miloš Pánek 

## Parametry systému
Model hromadné obsluhy $M | G | \infty$

Požadavky přicházejí podle Poissonova procesu s intenzitou $\lambda = 10 \space s^{-1}$

Doba obsluhy jednoho požadavku (v sekundách) má rozdělení $S \sim Ga(a,p)$, kde $a=4$ a $b=2$

Časy mezi příchody a časy obsluhy jsou nezávislé.

Systém má (teoreticky) nekonečně paralelních obslužných míst (každý příchozí je rovnou obsluhován).

## 1)

### Postup generování trajektorie

Vygenerujeme počet požadavků a časy jejich příchodů. Pro každý požadavek také vygenerujeme délku obsluhy a sečtením s časem příchodu dostáváme čas opuštění systému. Vytvoříme seznam událostí, slučující příchody a odchody požadavků. Pro každou událost podle jejího typu zvýšíme či snížíme aktuální počet požadavků v systému. Pokud je časem této události čas vyšší něž je horní hranice, generování ukončíme.

### Grafické znázornění trajektorie systému

![Znázornění jedné trajektorie systému](img/trajectory.png)

\pagebreak

### Kód pro generování trajektorie systému

```python
def simulate_trajectory(time_max, lambd, a, p):
    #počet požadavků během daného času generováno pomocí Poissonova rozdělení
    request_count = np.random.poisson(lambd * time_max)
    #časy příchodu jednotlivých požadavků
    requests_arrive = np.random.uniform(low=0, high=time_max, size=request_count)
    requests_arrive.sort()
    #doby trvání jednotlivých požadavků
    wait_times = np.random.gamma(p, 1/a, size=request_count)    
    #časy, kdy jednotlivé požadavky opouští systém
    requests_leave = requests_arrive + wait_times
    #společný seznam pro příchody a odchody
    event_list = []
    
    for arr in requests_arrive:
        event_list.append((arr, 1))
    
    for lev in requests_leave:
        event_list.append((lev, -1))

    event_list.sort(key=lambda a: a[0])
    
    count = 0
    data = [0]  #počet zákazníků v daném čase
    events=[0]  #časy změny počtu zákazníků
    #pro každou změnu počtu zákazníků si zaznamenáme počet aktuální zákazníků
    for event in event_list:
        if event[0] > time_max: break
        count+=event[1]
        assert(count >= 0)
        data.append(count)
        events.append(event[0])
    
    return events, data, data[-1]
```

Pro vykreslení trajektorie byla následně použita funkce ```step```.

## 2)

### Postup nalezení rozdělení

Pro nalezení rozdělení veličiny $N_{100}$ byla použita funkce ```fit``` z knihovny ```scipy.stats```. 

```python
def fit_curve(data):
    distributions = [st.gamma, st.expon, st.norm, st.uniform, st.t, st.poisson] 
    for distribution in distributions:
        plt.figure(figsize=(12, 8))
        #bounds zvoleny experimentálně
        res = st.fit(distribution, data, bounds=[(-50, 50)])
        print(distribution.name)
        print(res)
        res.plot()
        plt.savefig(f"img/fit_{distribution.name}.png")
```

\pagebreak

### Grafické znázornění rozdělení

expon  |  gamma
:-------------------------:|:---------------:
![](img/fit_expon.png){width=50%}  |  ![](img/fit_gamma.png){width=50%} 

norm  |  uniform
:-------------------------:|:---------------:
![](img/fit_norm.png){width=50%}  |  ![](img/fit_uniform.png){width=50%}

poisson  |  t 
:-------------------------:|:---------------:
![](img/fit_poisson.png){width=50%}  |  ![](img/fit_t.png){width=50%}

\pagebreak

### Výstup testování

```
gamma
    params: FitParams(a=5.034951344454789, loc=0.0, scale=1.0)
    success: False
    message: 'Optimization converged to parameter values that are inconsistent with the data.'
expon
    params: FitParams(loc=-0.00902665723532814, scale=1.0)
    success: True
    message: 'Optimization terminated successfully.'
norm
    params: FitParams(loc=5.113999982831798, scale=1.0)
    success: True
    message: 'Optimization terminated successfully.'
uniform
    params: FitParams(loc=3.2506818722555897, scale=1.0)
    success: False
    message: 'Optimization converged to parameter values that are inconsistent with the data.'
t
    params: FitParams(df=0.5532330787324429, loc=0.0, scale=1.0)
    success: True
    message: 'Optimization terminated successfully.'
poisson
    params: FitParams(mu=5.11400012116424, loc=0.0)
    success: True
    message: 'Optimization terminated successfully.'
```

Jako nejvhodnější se tedy jeví Poissonovo rozdělení s parametrem $\lambda \doteq 5$.

\pagebreak

## 3)

Rozdělení $N_t$ pro $t\rightarrow \infty$ je rovno Poissonovu rozdělení s intenzitou $\lambda / \mu = 10 / 2 = 5$.

Postupujeme pomocí testu dobré shody.

$H_0$ : Náhodná veličina $N_t \sim Poisson(5)$ pro $t\rightarrow \infty$

$H_A$ : Náhodná veličina $N_t \nsim Poisson(5)$ pro $t\rightarrow \infty$

```python
def test(data):
    #H0: N_t ~~ Poisson(5)
    #HA: N_t !~ Poisson(5)
    
    n = len(data)
    #teoretické četnosti    
    pi = np.zeros(shape=max(data), dtype="float")
    
    for p in range(0, max(data)):
        pi[p] = st.poisson.pmf(p, 5)
    pi = np.sort(pi)
    npi = pi * n
    
    #slučování binů pro teoretické četnosti aby pro všechna i npi > 5
    while npi[0] < 5:
        npi = [npi[0]+npi[1]]+npi[2:]
        
    k = len(npi)
    #rozdělení naměřených hodnot do stejného počtu binů
    counts, bin_edges = np.histogram(data, bins=k)
    counts = np.sort(counts) 
    #výpočet testové statistiky    
    sum = 0
    for i in range(len(counts)):
        sum += ((counts[i] -npi[i])**2)/npi[i]

    df = k - 1 - 1 #odhadli jsme jeden parametr rozdělení
    print(f"\nX2: {sum}")
    print(f"df: {df}")
```

Výsledek:

```
X2: 12.103313664074355
df: 10
```

Protože $\chi^2_{0.05, 10} \doteq 18.307 > 12.103$ nezamítáme nulovou hypotézu.

Test tedy vychází podle očekávání, i podle dříve nalezeného rozdělení v části 2)

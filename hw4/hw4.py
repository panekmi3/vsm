import numpy as np
np.set_printoptions(precision = 4, suppress = True)
import matplotlib.pyplot as plt
import scipy.stats as st
import math
import random

def simulate_trajectory(time_max, lambd, a, p):
    #počet požadavků během daného času generováno pomocé Poissonova rozdělení
    request_count = np.random.poisson(lambd * time_max)
    #časy příchodu jednotlivých požadavků
    requests_arrive = np.random.uniform(low=0, high=time_max, size=request_count)
    requests_arrive.sort()
    #doby trvání jednotlivých požadavků
    wait_times = np.random.gamma(p, 1/a, size=request_count)    
    #časy, kdy jednotlivé požadavky opouští systém
    requests_leave = requests_arrive + wait_times
    #společný seznam pro příchody a odchody
    event_list = []
    
    for arr in requests_arrive:
        event_list.append((arr, 1))
    
    for lev in requests_leave:
        event_list.append((lev, -1))

    event_list.sort(key=lambda a: a[0])
    
    count = 0
    data = [0]  #počet zákazníků v daném čase
    events=[0]  #časy změny počtu zákazníků
    #pro každou změnu počtu tákazníků si zaznamenáme počet aktuální zákazníků
    for event in event_list:
        if event[0] > time_max: break
        count+=event[1]
        assert(count >= 0)
        data.append(count)
        events.append(event[0])
    
    return events, data, data[-1]


def fit_curve(data):
    distributions = [st.gamma, st.expon, st.norm, st.uniform, st.t, st.poisson] 
    for distribution in distributions:
        plt.figure(figsize=(12, 8))
        #bounds zvoleny experimentálně
        res = st.fit(distribution, data, bounds=[(-50, 50)])
        print(distribution.name)
        print(res)
        res.plot()
        plt.savefig(f"img/fit_{distribution.name}.png")

def test(data):
    #H0: N_t ~~ Poisson(5)
    #HA: N_t !~ Poisson(5)
    
    n = len(data)
    #teoretické četnosti    
    pi = np.zeros(shape=max(data), dtype="float")
    
    for p in range(0, max(data)):
        pi[p] = st.poisson.pmf(p, 5)
    pi = np.sort(pi)
    npi = pi * n
    
    #slučování binů pro teoretické četnosti aby ∀i npi > 5
    while npi[0] < 5:
        npi = [npi[0]+npi[1]]+npi[2:]
        
    k = len(npi)
    #rozdělení naměřených hodnot do stejného počtu binů
    counts, bin_edges = np.histogram(data, bins=k)
    counts = np.sort(counts) 
    #výpočet testové statistiky    
    sum = 0
    for i in range(len(counts)):
        sum += ((counts[i] -npi[i])**2)/npi[i]

    print(f"\nX2: {sum}")
    print(f"df: {k-1-1}")


if __name__ == '__main__':
    
    trajectory, events, end = simulate_trajectory(10, 10, 4, 2)
    
    plt.figure(figsize=(12, 6))
    
    plt.step(trajectory, events, where='post')
    plt.xlabel("Čas [s]")
    plt.ylabel("Počet požadavků")
    plt.title("Průběh počtu požadavků")
    plt.savefig("img/trajectory.png")
    
    end_states = []
    
    for i in range(500):
        end_states.append(simulate_trajectory(100, 10, 4, 2)[2])

    fit_curve(end_states)
    test(end_states)

 
    
    
    